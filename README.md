# DEPRECATED

This version of the SDK is no longer maintained.

From now on, in order to implement the Revolut Pay SDK, please use [Revolut Payments](https://bitbucket.org/revolut/revolut-payments-ios/src/master/).


## Migration Guide
Migrating to the new version is easy and fast. You must only follow these two steps:

1.  In your `Podfile`, change the following line from:
```ruby
pod 'RevolutPay'
```
to:
```ruby
pod 'RevolutPayments/RevolutPay'
```

2. In your Xcode project, modify the imports of `RevolutPay` to `RevolutPayments`.

---

# Revolut Pay: iOS SDK

The Revolut Pay SDK for iOS lets you accept Revolut Pay payments directly from your app. 

The SDK allows you to create a Revolut Pay button which enables your users to pay through Revolut Pay for their items.

The integration is straightforward and fast.

## Get started

You must follow these steps to support Revolut Pay in your app:

1. [Installation](#markdown-header-installation)
2. [Support the Revolut app URL scheme](#markdown-header-support-the-revolut-app-url-scheme)
3. [Configure the SDK](#markdown-header-configure-the-sdk)
4. [Create a Revolut Pay button](#markdown-header-create-a-revolut-pay-button)
5. [Display the button](#markdown-header-display-the-button)

You can also check out our [Example project](ExampleApp) to test out the SDK and understand how to implement it.

> **Tip:** Refer to the SDK's Xcode reference documentation for detailed usage of any component and more explanations.

## Installation

Minimum supported iOS version: 13.0

### CocoaPods

To integrate `RevolutPay` into your Xcode project through [CocoaPods](https://cocoapods.org), add the following pod in your `Podfile`:

```ruby
pod 'RevolutPay'
```

### Manual installation

Drag and drop `RevolutPay.xcframework` and `RevolutPay.bundle` into your project. 

Embed the framework without signing.  

## Support the Revolut app URL scheme

Beginning with iOS 9 and Xcode 7, apps must declare the URL schemes that they intend to open, by specifying the schemes in the app’s ``Info.plist`` file.

Our SDK opens the Revolut mobile app (if installed) when the user taps the Revolut Pay button, and your app therefore needs to declare the relevant URL scheme.

```xml
<key>LSApplicationQueriesSchemes</key>
<array>
    <string>revolut</string>
</array>
```

## Configure the SDK

Import the ``RevolutPay`` module with:

```swift
import RevolutPay
```

Before proceeding with any SDK usage, you must first configure the SDK by calling `RevolutPayKit.configure(with:)`. 

This step can be done on app launch, in `AppDelegate`.

> **Note**: You need to first generate the [Merchant API key](https://developer.revolut.com/docs/accept-payments/get-started/generate-the-api-key/). To do this, you must have been [accepted as a Merchant](https://www.revolut.com/business/help/merchant-accounts/getting-started/how-do-i-apply-for-a-merchant-account) in your [Revolut Business](https://business.revolut.com/merchant) account. The iOS SDK requires the **public** merchant key.


## Create a Revolut Pay button

You must first create a `RevolutPayKit` object. It provides all the necessary tools for processing the payment of a specific item or list of items (for example, the items in your Checkout screen).

```swift
let revolutPayKit = RevolutPayKit()
```

To create a Revolut Pay button, call the `button` or `swiftUIButton` method of the object. Through the function parameters, you are able to configure the visual style of the button and handle all the steps of the payment flow.

```swift
let button = revolutPayKit.button(
  style: .init(size: .large),
  returnURL: "myapp://revolut-pay", // <- Change with your actual return URL
  createOrder: { createOrderHandler in
      // Get the order token from your back end
      createOrderOnBackEnd { orderToken in
          createOrderHandler.set(orderToken: orderToken)
      }
  },
  completion: { result in
      switch result {
      case .success:
          // Handle successful payment
      case .failure(let error):
          // Handle error
      }
  }
)
```


## Display the button

To display the button, you must simply insert it into your view hierarchy.
For example:

```swift
view.addSubview(button)
button.translatesAutoresizingMaskIntoConstraints = false
NSLayoutConstraint.activate([
    button.bottomAnchor.constraint(equalTo: view.bottomAnchor),
    button.leadingAnchor.constraint(greaterThanOrEqualTo: view.leadingAnchor),
    button.trailingAnchor.constraint(lessThanOrEqualTo: view.trailingAnchor),
    button.centerXAnchor.constraint(equalTo: view.centerXAnchor)
])
```

If all the steps have been followed correctly, you have successfully implemented Revolut Pay! :tada:

## Parameters

### Button style

You can customize the Revolut Pay button’s size, color scheme (for both light and dark mode), radius and display the cashback incentive attachment.

### Return URL

As can be seen from the code snippet above, a return URL which the Revolut app will try to open upon payment completion is expected.

You must provide a [Universal Link](https://developer.apple.com/documentation/xcode/allowing-apps-and-websites-to-link-to-your-content?preferredLanguage=occ) or [scheme based URL](https://developer.apple.com/documentation/xcode/defining-a-custom-url-scheme-for-your-app) of your app.

For example, your return URL can look similar to one of these:

- Universal Link: `https://www.${my_app_name}.com/revolut-pay`
- URL scheme: `${my_app_name}://revolut-pay`

> **Attention:** Please ensure the return URL you pass is valid.

### Order creation callback

As can be seen from the Revolut Pay button creation code snippet, you are responsible for providing the order token to the SDK through a callback. It is executed after the user taps on the Revolut Pay button. In this callback:

1. Your app should make a request to your back end.
2. Your back end would in turn respond with the token of the order that the user wants to pay for.
3. Then, you simply pass the token to the SDK through the `CreateOrderHandler` callback parameter. You must always pass a new order token, even for the same checkout session!

> **Note**: For the back end integration, please refer to the [REST API documentation](https://developer.revolut.com/docs/api-reference/merchant/#operation/createOrder).

> **Note:** The order token might also be referred to as `public_id` by the back end.

### Completion

The completion closure is called once upon payment completion or failure. It will always be called, even if the user abandons the payment.

If the result is `success`, the user has successfully finalized the payment. If the result is `failure`, you should treat it accordingly.

Please note that according to your UX guidelines, you might or might not want to ignore the `userAbandonedPayment` error case, as it is thrown even when the user simply dismisses the web view flow, for example. 

Also, please consider that the completion might be called with failure even though the payment succeeds afterwards. For example, if the user abandons the payment or polling fails because of an internal reason, they might still be able to pay for the order later, even though the SDK notified you that payment has failed. In order to avoid duplicate payments, you can check with your own back end whether the order was paid for successfully and then act accordingly.

## Verify your implementation

### Sandbox
To test your implementation, you can use the [sandbox environment](https://developer.revolut.com/docs/accept-payments/tutorials/test-in-the-sandbox-environment/configuration/).

To do this, pass the `.sandbox` environment parameter when configuring the SDK through `RevolutPayKit.configure(with:)`.

There are two scenarios you can test:

1. When you have the [Revolut app](https://apps.apple.com/app/revolut/id932493382) installed and you try to make a payment, an alert will be displayed that substitutes the Revolut app. In production, the SDK will redirect you to the Revolut app.
 Please make sure that you also verify your implementation in the production environment, so you can tell if the redirection to and from your app is correct.
2. When you do not have it installed, the web view flow will be opened.

> **Attention:** Make sure to only release your app with the `.production` environment set!

### Production
It is equally important to check that the payment flow works as expected in the production environment.

:warning: Critical things to check:

1. When you have the [Revolut app](https://apps.apple.com/app/revolut/id932493382) installed and you try to make a payment, you must get redirected to the payment flow in the Revolut app. If you don’t, it is possible that you did not [add support for the Revolut URL scheme](#markdown-header-support-the-revolut-app-url-scheme) correctly.
2. Upon a payment completion (even if it failed) from the Revolut app, you must get redirected successfully back to your app. If you don't, it is possible that the [return URL](#markdown-header-return-url) you passed to the SDK is incorrect.
3. If you do not have the Revolut app installed, a web view flow gets opened directly in your app.