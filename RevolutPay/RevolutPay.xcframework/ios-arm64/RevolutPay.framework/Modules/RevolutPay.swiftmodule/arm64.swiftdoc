✨  )   8,@��
�P�P
�@
�0
� 
��
��
A�Ќ�+�-��+��/�+��(��C�B)�B(�B(�B(<0B+�)�+��/�B(�B)<-��,�B)��A�(�B+�B)�-��(��+��*<8)��(��/��+�B+��+��,<0�)��+�,��+�B+��, �	  �  %  "   Tf�� 0"��    �   Apple Swift version 5.5.2 (swiftlang-1300.0.47.5 clang-1300.0.29.30)T   RevolutPay  �   arm64-apple-ios13.0     �  �  2J�A�       @?��4   �  s:10RevolutPay0aB3KitC5ErrorO15kitAlreadyInUseyA2EmFI   Returned when the RevolutPayKit instance is already being used by a flow.   	   R   /// Returned when the ``RevolutPayKit`` instance is already being used by a flow.
	      ///
	   �   /// For example, if you created multiple buttons with the same ``RevolutPayKit`` instance and the user tries to press on a button while the order callback is running for another button, this error is returned.
        p��     s:10RevolutPay0aB6ButtonC5StyleV4size8variants6radius010attachmentD0A2E4SizeO_AE12VariantModesVAE6RadiusOAE010AttachmentD0VSgtcfc"   Creates a RevolutPayButton object.
   	   +   /// Creates a ``RevolutPayButton`` object.
	      /// - Parameters:
	   |   ///   - size: The size of the button. Based on it, the button's intrinsic content size is bigger or smaller (as a result of
	      ///    different underlying font sizes, constraints etc.). You should use a size that fits your needs. Ideally, you should use
	   |   ///    the intrinsic content size, but in case you add constraints to the view such as width or height, you must be careful
	   R   ///    not to deform the button and keep it visually appealing. Default: `.large`
	   n   ///   - variants: The light/dark mode variants of the button. The button's color scheme automatically changes
	   _   ///   depending on the phone's appearance. Please choose the appropriate colors for each mode.
	   >   ///   - radius: The radius of the button. Default: ``.small``
	   �   ///   - attachmentStyle: The button can have an attached box to it, which tries to attract more users to pay. This describes its style.
        �'m(   �   c:@M@RevolutPay@objc(cs)RevolutPayButtonS   A view that contains a button and other elements for starting the Revolut Pay flow.      X   /// A view that contains a button and other elements for starting the Revolut Pay flow.
         ��O�<   �  s:10RevolutPay0aB3KitC9configure4withyAC13ConfigurationV_tFZ�   Initializes RevolutPayKit with the required configuration, provided by the merchant. Must be called before proceeding with any other usage.      ]   /// Initializes ``RevolutPayKit`` with the required configuration, provided by the merchant.
   ?   /// **Must be called before proceeding with any other usage.**
      ///
   ?   /// - Parameter configuration: Configuration for using the SDK
        HI��   �	  s:10RevolutPay0aB3KitC6button5style9returnURL11createOrder10completionAA0aB6ButtonCAJ5StyleV_SSyAC06CreateI7HandlerCcys6ResultOyytAC5ErrorOGctFI   Creates a Revolut Pay button associated with this RevolutPayKit instance.      R   /// Creates a Revolut Pay button associated with this ``RevolutPayKit`` instance.
      ///
   }   /// Once tapped, the SDK opens the Revolut app if installed on the user's device or starts a web view based flow, otherwise.
      ///
   V   /// The `completion` closure is eventually called with either `success` or ``Error``.
      ///
   w   /// - Note: Internally, the SDK polls the payment state of the order. Please consider that there are non-obvious cases
   {   ///  where the user might abandon the payment (for example, killing the Revolut app). To tackle these cases, polling times
   r   ///  out after a certain amount of time. If for some reason, you require to stop polling manually, you should use
   %   ///  ``stopPollingPaymentResponse``.
      /// - Parameters:
   /   ///   - style: The visual style of the button.
   m   ///   - returnURL: The URL which the Revolut app tries to open upon payment completion. You should provide a
   6   ///   Universal Link or scheme based URL of your app.
   }   ///   - createOrder: The callback that allows you to create an order and configure the Revolut Pay flow for it. For example,
   �   ///    during this callback, your app might create a Revolut Pay order through a back end request. It would then pass the order
   A   ///    token to the SDK. You must always pass a new order token!
   �   ///   - completion: The closure called upon payment completion. If the result is `success`, the user has successfully finalized
   �   ///    the payment. If the result is `failure`, you should treat it accordingly. Please note that according to your UX guidelines, you
   z   ///    might or might not want to ignore the `userAbandonedPayment` error case, as it is thrown even when the user simply
   �   ///    dismisses the web view flow, for example. Also, please consider that the completion might be called with failure even though
   �   ///    the payment succeeds afterwards. For example, if the user abandons the payment or polling fails because of an internal reason,
   �   ///    they might still be able to pay for the order later, even though the SDK notified you that payment has failed. In order to avoid
   �   ///    duplicate payments, you can check with your own back end whether the order was paid for successfully and then act accordingly.
   ,   /// - Returns: Styled ``RevolutPayButton``.
        Kw�
@   �   s:10RevolutPay0aB6ButtonC05innerC23AccessibilityIdentifierSSSgvp5   Accessibility identifier of the actual button subview      :   /// Accessibility identifier of the actual button subview
        �A�6   �   s:10RevolutPay0aB3KitC11EnvironmentO10productionyA2EmF   Used for production   	      /// Used for production
	      ///
	   /   /// - Note: Must be used with production keys.
       �&j5   �   s:10RevolutPay0aB3KitC5ErrorO16sdkNotConfiguredyA2EmF(   Returned when the SDK is not configured.   	   -   /// Returned when the SDK is not configured.
	      ///
	   T   /// Please make sure that you have first called ``RevolutPayKit.configure(with:)``.
        �'�    k   s:10RevolutPay0aB6ButtonC5StyleV%   The visual style of RevolutPayButton.      .   /// The visual style of ``RevolutPayButton``.
         ��i�3     s:10RevolutPay0aB3KitC5ErrorO03apiD0yAEsAD_pSgcAEmF-   Returned when an internal API error occurred.   	   2   /// Returned when an internal API error occurred.
	      ///
	   z   /// There might be various reasons, including the case where the merchant passes an order token of an already paid order.
       ��f:   3   s:10RevolutPay0aB6ButtonC5StyleV4TextV8CurrencyO3usdyA2ImF   U.S. dollar         /// U.S. dollar
       U�	-:   %   s:10RevolutPay0aB6ButtonC5StyleV4TextV8CurrencyO3euryA2ImF   Euro      	   /// Euro
    
    ׆O�=   �   s:10RevolutPay0aB6ButtonC5StyleV7VariantO12darkOutlinedyA2GmF#   Dark background with light outline.      (   /// Dark background with light outline.
      ///
   R   /// - Note: Should only be used on light backgrounds because of the dark outline.
       �>��C     s:10RevolutPay0aB6ButtonC5StyleV12VariantModesV7anyModeAgE0E0O_tcfcL   Constructs an object where the same variant is used for both light and dark.      Q   /// Constructs an object where the same variant is used for both light and dark.
   a   /// - Parameter anyMode: The variant that is used for both light and dark mode. Default: `.dark`
        ��bD   �   s:10RevolutPay0aB6ButtonC5StyleV4TextV13CashbackValueO7value10yA2ImF4   Ten units of the currency you specified. (e.g. £10)      9   /// Ten units of the currency you specified. (e.g. £10)
        �L-a�   �	  s:10RevolutPay0aB3KitC13swiftUIButton5style9returnURL11createOrder10completionQrAA0aB6ButtonC5StyleV_SSyAC06CreateJ7HandlerCcys6ResultOyytAC5ErrorOGctFQ   Creates a SwiftUI Revolut Pay button associated with this RevolutPayKit instance.      ^   /// Creates a ``SwiftUI`` Revolut Pay button associated with this ``RevolutPayKit`` instance.
      ///
   }   /// Once tapped, the SDK opens the Revolut app if installed on the user's device or starts a web view based flow, otherwise.
      ///
   V   /// The `completion` closure is eventually called with either `success` or ``Error``.
      ///
   w   /// - Note: Internally, the SDK polls the payment state of the order. Please consider that there are non-obvious cases
   {   ///  where the user might abandon the payment (for example, killing the Revolut app). To tackle these cases, polling times
   r   ///  out after a certain amount of time. If for some reason, you require to stop polling manually, you should use
   %   ///  ``stopPollingPaymentResponse``.
      /// - Parameters:
   /   ///   - style: The visual style of the button.
   m   ///   - returnURL: The URL which the Revolut app tries to open upon payment completion. You should provide a
   6   ///   Universal Link or scheme based URL of your app.
   }   ///   - createOrder: The callback that allows you to create an order and configure the Revolut Pay flow for it. For example,
   �   ///    during this callback, your app might create a Revolut Pay order through a back end request. It would then pass the order
   A   ///    token to the SDK. You must always pass a new order token!
   �   ///   - completion: The closure called upon payment completion. If the result is `success`, the user has successfully finalized
   �   ///    the payment. If the result is `failure`, you should treat it accordingly. Please note that according to your UX guidelines, you
   z   ///    might or might not want to ignore the `userAbandonedPayment` error case, as it is thrown even when the user simply
   �   ///    dismisses the web view flow, for example. Also, please consider that the completion might be called with failure even though
   �   ///    the payment succeeds afterwards. For example, if the user abandons the payment or polling fails because of an internal reason,
   �   ///    they might still be able to pay for the order later, even though the SDK notified you that payment has failed. In order to avoid
   �   ///    duplicate payments, you can check with your own back end whether the order was paid for successfully and then act accordingly.
   9   /// - Returns: Styled ``RevolutPayButton`` wrapped view.
       ��ʈ   a  s:10RevolutPay0aB3KitC�   A kit that provides all the necessary tools for processing the payment of a specific item or list of items (for example, the items in your Checkout screen).      o   /// A kit that provides all the necessary tools for processing the payment of a specific item or list of items
   6   /// (for example, the items in your Checkout screen).
         �hB5   �   s:10RevolutPay0aB6ButtonC5StyleV7VariantO5lightyA2GmF   Light background.         /// Light background.
      ///
   5   /// - Note: Should only be used on dark backgrounds.
        "+�:   7   s:10RevolutPay0aB6ButtonC5StyleV4TextV8CurrencyO3gbpyA2ImF   British pound         /// British pound
        �ږ�4   �   s:10RevolutPay0aB6ButtonC5StyleV7VariantO4darkyA2GmF   Dark background.         /// Dark background.
      ///
   6   /// - Note: Should only be used on light backgrounds.
        e7�GC   �   s:10RevolutPay0aB6ButtonC5StyleV010AttachmentD0V4textAgE4TextV_tcfc   Constructs an attachment style.   	   $   /// Constructs an attachment style.
	   B   /// - Parameter text: Specifies the text style of the attachment.
        �Ϫs9   4  s:10RevolutPay0aB3KitC5ErrorO20userAbandonedPaymentyA2EmF=   Returned when the user abandons the Revolut Pay payment flow.   	   B   /// Returned when the user abandons the Revolut Pay payment flow.
	      ///
	   -   /// Examples of when this error is returned:
	   <   /// - a specific time after the user kills the Revolut app.
	   .   /// - when the user cancels the web view flow
	      ///
	   �   /// - Note: According to your UX guidelines, you might or might not want to ignore the `userAbandonedPayment` error case, as it is thrown even when the user simply dismisses the web view flow, for example.
        �-�D5   �   s:10RevolutPay0aB3KitC18CreateOrderHandlerC6cancelyyFn   Stops the flow in case the merchant app decides that it should not be continued anymore at this specific step.      s   /// Stops the flow in case the merchant app decides that it should not be continued anymore at this specific step.
        �Ք.A     s:10RevolutPay0aB3KitC18CreateOrderHandlerC3set10orderTokenySS_tFA   Sets the token of the order that the flow will be configured for.      F   /// Sets the token of the order that the flow will be configured for.
      ///
   \   /// The merchant's app makes a request to its back end which responds with the order token.
   O   /// The order token is then passed to the Revolut Pay SDK through this method.
      ///
   X   /// - Note: You must always pass a new order token, even for the same checkout session!
   >   /// - Parameter orderToken: order's token / public identifier
        �L 2   �   s:10RevolutPay0aB3KitC11EnvironmentO7sandboxyA2EmF$   Used for testing your implementation   	   )   /// Used for testing your implementation
	      ///
	   ,   /// - Note: Must be used with sandbox keys.
    	    l�3�&   �   s:10RevolutPay0aB3KitC13ConfigurationV?   Configuration structure that facilitates the SDK initalization.      D   /// Configuration structure that facilitates the SDK initalization.
         ����F   �  s:10RevolutPay0aB3KitC5ErrorO09cantBuildA13UniversalLinkyAESS_SStcAEmF�   Returned when the SDK could not build internally the Universal Link that is used for redirecting to the Revolut app or for opening the web view flow.   	      /// Returned when the SDK could not build internally the Universal Link that is used for redirecting to the Revolut app or for
	      /// opening the web view flow.
	      ///
	   L   /// Please make sure that the token and return URL are correctly formatted.
        n�>�0   �   s:10RevolutPay0aB6ButtonC5StyleV4TextV8CurrencyO,   The currency displayed in the cashback text.   	   1   /// The currency displayed in the cashback text.
	      ///
	   t   /// - Note: You must select the appropriate value. Please reach out to your business team for the negotiated value.
    	   �=T!>   �   s:10RevolutPay0aB6ButtonC5StyleV7VariantO13lightOutlinedyA2GmF#   Light background with dark outline.      (   /// Light background with dark outline.
      ///
   R   /// - Note: Should only be used on dark backgrounds because of the light outline.
        qlS~2   �  s:10RevolutPay0aB3KitC23stopPollingPaymentStateyyF+   Forcefully stops polling the payment state.	      0   /// Forcefully stops polling the payment state.
      ///
   {   /// Internally, the ``RevolutPayKit`` instance polls the payment state of the order. There are non-obvious cases where the
   �   /// user might abandon the payment (for example, killing the Revolut app). To tackle these cases, polling times out after a certain
      /// amount of time.
      ///
   �   /// If instead of declaring the ``RevolutPayKit`` instance in your checkout screen, you declared it globally (e.g. a singleton)
   ~   /// and user abandons the flow in a non-obvious way, you can either stop polling manually by calling this method or leave the
   *   /// polling continue until it times outs.
        s^si2   �   s:10RevolutPay0aB3KitC5ErrorO13paymentFailedyA2EmF    Returned when the payment fails.   	   %   /// Returned when the payment fails.
	      ///
	   Y   /// For example, the user might have insufficient funds which in turn fails the payment.
       ��S�O   r  s:10RevolutPay0aB6ButtonC5StyleV12VariantModesV9lightMode04darkH0AgE0E0O_AKtcfcQ   Constructs an object with specific variants for light and dark mode respectively.      V   /// Constructs an object with specific variants for light and dark mode respectively.
      /// - Parameters:
   E   ///   - lightMode: The variant used for light mode. Default: `.dark`
   D   ///   - darkMode: The variant used for dark mode. Default: `.light`
        t�+   q   s:10RevolutPay0aB3KitC18CreateOrderHandlerC*   Handler class for the order creation step.      /   /// Handler class for the order creation step.
         :�H_   �   s:10RevolutPay0aB3KitC13ConfigurationV17merchantPublicKey11environmentAESS_AC11EnvironmentOtcfc       	   8   /// - Parameter mercantPublicKey: Merchant's public key
	   3   /// - Parameter environment: The environment type.
        �LL�6   9  s:10RevolutPay0aB6ButtonC5StyleV4TextV13CashbackValueOJ   The maximum cashback value in the currency that you specified. (e.g. £25)   	   O   /// The maximum cashback value in the currency that you specified. (e.g. £25)
	      ///
	   t   /// - Note: You must select the appropriate value. Please reach out to your business team for the negotiated value.
        ? ��D   �   s:10RevolutPay0aB6ButtonC5StyleV4TextV13CashbackValueO7value25yA2ImF<   Twenty-five units of the currency you specified. (e.g. £25)      A   /// Twenty-five units of the currency you specified. (e.g. £25)
          @   $      �                      �  d	          �  �  x                                    ;      �  �  �(                          i)      �)  �*      �+  ".  ^/  �1      �2  y3  �5          �7      ;  �=                      �>      }?          �@  "
h!               