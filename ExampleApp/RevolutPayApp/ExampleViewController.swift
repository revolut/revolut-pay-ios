//
//  ExampleViewController.swift
//  RevolutPayApp
//
//  Created by Revolut on 08.07.2022.
//

import UIKit
import RevolutPay

final class ExampleViewController: BaseViewController {
    // MARK: - Private properties
    private let revolutPayKit = RevolutPayKit()
    private let completionPresenter = CompletionPresenter()
    private lazy var revolutPayButton: RevolutPayButton = makeRevolutPayButton()
}

// MARK: - View lifecycle
extension ExampleViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Checkout"
        configureRevolutPay()
        addRevolutPayButton()
        addProductsPlaceholder()
    }
}

// MARK: - Private methods
private extension ExampleViewController {
    // MARK: - SDK Configuration
    // This step can be done in your AppDelegate.
    func configureRevolutPay() {
        // Besides configuring the SDK through the following method, please make sure that
        // you have added the `revolut` scheme to the app's supported schemes as explained in the documentation.
        // The Example project already has it added in the Info.plist file.

        RevolutPayKit.configure(
            with: .init(
                merchantPublicKey: Defaults.merchantPublicKey,
                environment: Defaults.environment
            )
        )
    }

    // MARK: - Revolut Pay button creation
    func makeRevolutPayButton() -> RevolutPayButton {
        revolutPayKit.button(
            style: .init(
                variants: .init(
                    lightMode: .lightOutlined,
                    darkMode: .darkOutlined
                )
            ),
            // The Example project already supports the `exampleapp` URL scheme.
            returnURL: Defaults.returnURL,
            createOrder: { createOrderHandler in
                // Simulating a network request to your back end which must return the order token.
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                    guard let orderToken = Defaults.orderToken else {
                        createOrderHandler.cancel()
                        return
                    }
                    createOrderHandler.set(orderToken: orderToken)
                }
            },
            completion: { [weak self] result in
                self?.completionPresenter.showCompletionAlert(for: result)
                print(result)
            }
        )
    }

    // MARK: - Adding Revolut Pay button in view hierarchy
    func addRevolutPayButton() {
        view.addSubview(revolutPayButton)

        revolutPayButton.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            revolutPayButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -30),
            revolutPayButton.trailingAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.trailingAnchor),
            revolutPayButton.leadingAnchor.constraint(greaterThanOrEqualTo: view.safeAreaLayoutGuide.leadingAnchor),
            revolutPayButton.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }

    func addProductsPlaceholder() {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .placeholderViewGray

        self.view.addSubview(view)

        NSLayoutConstraint.activate([
            view.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 30),
            view.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -30),
            view.bottomAnchor.constraint(equalTo: revolutPayButton.topAnchor, constant: -30),
            view.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 30)
        ])
    }
}
