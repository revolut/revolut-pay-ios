//
//  ExampleViewController.swift
//  RevolutPayApp
//
//  Created by Revolut on 08.07.2022.
//

import UIKit
import RevolutPay

protocol CompletionPresenterProtocol {
    func showCompletionAlert(for result: Result<Void, RevolutPayKit.Error>)
}

final class CompletionPresenter: CompletionPresenterProtocol {
    func showCompletionAlert(for result: Result<Void, RevolutPayKit.Error>) {
        let title: String = {
            switch result {
            case .success:
                return "Successful payment 🎉"
            case .failure:
                return "Payment failed 😪"
            }
        }()

        let message: String = {
            switch result {
            case .success:
                return "User has successfully paid through Revolut Pay."
            case .failure:
                return "An error has occurred."
            }
        }()

        let alertController = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert
        )
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        UIApplication.shared.keyWindowPresentedController?
            .present(alertController, animated: true, completion: nil)
    }
}

private extension UIApplication {
    var keyWindow: UIWindow? {
        UIApplication.shared.windows.first(where: \.isKeyWindow)
    }

    var keyWindowPresentedController: UIViewController? {
        let viewController = keyWindow?.rootViewController

        if let presentedController = viewController as? UITabBarController {
            return presentedController.selectedViewController?.topViewController
        }

        return viewController?.topViewController
    }
}

private extension UIViewController {
    var topViewController: UIViewController {
        var viewController: UIViewController? = self

        while let presentedController = viewController?.presentedViewController {
            if let presentedController = presentedController as? UITabBarController {
                viewController = presentedController.selectedViewController
            } else {
                viewController = presentedController
            }
        }

        return viewController ?? self
    }
}
