//
//  UIColor+Utils.swift
//  RevolutPayApp
//
//  Created by Revolut on 09.07.2022.
//

import UIKit

extension UIColor {
    static var placeholderViewGray: UIColor {
        return UIColor(named: "placeholderViewGray")!
    }
}
