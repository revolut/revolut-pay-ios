//
//  ConfigurationViewController.swift
//  RevolutPayApp
//
//  Created by Revolut on 07.07.2022.
//

import UIKit
import RevolutPay

final class ConfigurationViewController: BaseViewController {
    private let subviewFactory: ConfigurationSubviewFactoryProtocol
    private let environments: [RevolutPayKit.Environment] = [.production, .sandbox]
    private var selectedEnvironment = Defaults.environment
    private lazy var environmentSwitcher = makeEnvironmentSwitcher()
    private lazy var merchantKeyTextField = makeMerchantKeyTextField()
    private lazy var orderTokenTextField = makeOrderTokenTextField()
    private lazy var scrollView: UIScrollView = makeScrollView()
    private lazy var updateButton = makeUpdateButton()

    init(subviewFactory: ConfigurationSubviewFactoryProtocol) {
        self.subviewFactory = subviewFactory
        super.init(nibName: nil, bundle: nil)
    }

    convenience init() {
        self.init(subviewFactory: ConfigurationSubviewFactory())
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ConfigurationViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
        setupHierarchy()
        setupConstraints()
    }
}

private extension ConfigurationViewController {
    func setupViews() {
        navigationController?.navigationBar.prefersLargeTitles = true
        title = "Configure SDK"
        view.backgroundColor = .systemBackground
        validate()
    }

    func setupHierarchy() {
        view.addSubview(scrollView)
        view.addSubview(updateButton)
    }

    func setupConstraints() {
        NSLayoutConstraint.activate([
          scrollView.topAnchor.constraint(equalTo: view.topAnchor),
          scrollView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
          scrollView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor)
        ])

        NSLayoutConstraint.activate([
            updateButton.topAnchor.constraint(lessThanOrEqualTo: scrollView.bottomAnchor),
            updateButton.trailingAnchor.constraint(
                equalTo: view.safeAreaLayoutGuide.trailingAnchor,
                constant: -25
            ),
            updateButton.safeAreaLayoutGuide.bottomAnchor.constraint(
                equalTo: view.safeAreaLayoutGuide.bottomAnchor,
                constant: -30
            ),
            updateButton.leadingAnchor.constraint(
                equalTo: view.safeAreaLayoutGuide.leadingAnchor,
                constant: 25
            )
        ])
    }

    func makeEnvironmentSwitcher() -> UISegmentedControl {
        let environmentSwitcher = subviewFactory.makeEnvironmentSwitcher(items: ["Production", "Sandbox"])
        environmentSwitcher.addTarget(self, action: #selector(handleEnvironmentChange(_:)), for: .valueChanged)
        environmentSwitcher.selectedSegmentIndex = environments
            .firstIndex(where: { $0 == selectedEnvironment }) ?? -1
        return environmentSwitcher
    }

    func makeMerchantKeyTextField() -> UITextField {
        let merchantKeyTextField = subviewFactory.makeTextField(
            text: Defaults.merchantPublicKey,
            placeholder: "pk_xxxxxxxxxxxxxxxxxxxxxxxxxx"
        )
        merchantKeyTextField.addTarget(self, action: #selector(handleTextFieldEdit(_:)), for: .editingChanged)
        return merchantKeyTextField
    }

    func makeOrderTokenTextField() -> UITextField {
        let orderTokenTextField = subviewFactory.makeTextField(
            text: Defaults.orderToken,
            placeholder: "Order token"
        )
        orderTokenTextField.addTarget(self, action: #selector(handleTextFieldEdit(_:)), for: .editingChanged)
        return orderTokenTextField
    }

    func makeScrollView() -> UIScrollView {
        subviewFactory.makeScrollView(withSubviews: [
            subviewFactory.makeGroup(
                header: subviewFactory.makeHeader(text: "Environment"),
                content: environmentSwitcher,
                footer: nil
            ),
            subviewFactory.makeGroup(
                header: subviewFactory.makeHeader(text: "Merchant public key"),
                content: merchantKeyTextField,
                footer: nil
            ),
            subviewFactory.makeGroup(
                header: subviewFactory.makeHeader(text: "Order token"),
                content: orderTokenTextField,
                footer: subviewFactory.makeFooter(
                    text: """
                    You can provide a token through this field or leave it empty to \
                    simulate the cancel method. The order token should be retrieved from \
                    your server on Revolut Pay button tap in an actual implementation.
                    """
                )
            )
        ])
    }

    func makeUpdateButton() -> Button {
        let updateButton = subviewFactory.makeUpdateButton()
        updateButton.addTarget(self, action: #selector(handleUpdateButtonTap), for: .touchUpInside)
        return updateButton
    }

    func validate() {
        updateButton.isEnabled = merchantKeyTextField.text?.isEmpty == false
    }
}

private extension ConfigurationViewController {
    @objc func handleEnvironmentChange(_ segmentedControl: UISegmentedControl) {
        selectedEnvironment = environments[segmentedControl.selectedSegmentIndex]
    }

    @objc func handleUpdateButtonTap(_ button: UIButton) {
        guard let merchantPublicKey = merchantKeyTextField.text else { return }
        var orderToken = orderTokenTextField.text?.trimmingCharacters(in: .whitespaces)
        orderToken = orderToken?.isEmpty == true ? nil : orderToken
        Defaults.orderToken = orderToken
        Defaults.merchantPublicKey = merchantPublicKey.trimmingCharacters(in: .whitespaces)
        Defaults.environment = selectedEnvironment
        navigationController?.popViewController(animated: true)
        navigationController?.pushViewController(ExampleViewController(), animated: true)
    }

    @objc func handleTextFieldEdit(_ textField: UITextField) {
        validate()
    }
}
