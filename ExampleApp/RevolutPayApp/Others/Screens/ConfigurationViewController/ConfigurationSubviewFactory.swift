//
//  ConfigurationSubviewFactory.swift
//  RevolutPayApp
//
//  Created by Revolut on 08.07.2022.
//

import UIKit
import RevolutPay

protocol ConfigurationSubviewFactoryProtocol {
    func makeScrollView(withSubviews subviews: [UIView]) -> UIScrollView

    func makeTextField(text: String?, placeholder: String) -> UITextField

    func makeFooter(text: String) -> UILabel

    func makeHeader(text: String) -> UILabel

    func makeGroup(
        header: UILabel,
        content: UIView,
        footer: UILabel?
    ) -> UIStackView

    func makeEnvironmentSwitcher(items: [String]) -> UISegmentedControl

    func makeUpdateButton() -> Button
}

final class ConfigurationSubviewFactory: ConfigurationSubviewFactoryProtocol {
    func makeHeader(text: String) -> UILabel {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 20)
        label.text = text
        label.numberOfLines = 0
        return label
    }

    func makeFooter(text: String) -> UILabel {
        let label = UILabel()
        label.text = text
        label.font = .systemFont(ofSize: 16)
        label.textColor = .systemGray
        label.numberOfLines = 0
        return label
    }

    func makeGroup(
        header: UILabel,
        content: UIView,
        footer: UILabel?
    ) -> UIStackView {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 15
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(header)
        stackView.addArrangedSubview(content)
        if let footer = footer {
            stackView.addArrangedSubview(footer)
        }

        return stackView
    }

    func makeTextField(text: String?, placeholder: String) -> UITextField {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            textField.heightAnchor.constraint(equalToConstant: 50)
        ])

        textField.text = text

        textField.borderStyle = .roundedRect
        textField.backgroundColor = .systemBackground
        textField.tintColor = .label
        textField.textColor = .label
        textField.placeholder = placeholder
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
        textField.spellCheckingType = .no
        textField.smartDashesType = .no
        textField.smartQuotesType = .no

        return textField
    }

    func makeEnvironmentSwitcher(items: [String]) -> UISegmentedControl {
        let segmentedControl = UISegmentedControl(items: items)
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        return segmentedControl
    }

    func makeScrollView(withSubviews subviews: [UIView]) -> UIScrollView {
        let scrollView = UIScrollView()
        let contentView = UIView()
        let stackView = UIStackView()

        scrollView.contentInset = .init(top: 0, left: 0, bottom: 30, right: 0)

        subviews.forEach {
            stackView.addArrangedSubview($0)
        }
        stackView.spacing = 30
        stackView.axis = .vertical

        scrollView.addSubview(contentView)
        contentView.addSubview(stackView)

        scrollView.translatesAutoresizingMaskIntoConstraints = false
        contentView.translatesAutoresizingMaskIntoConstraints = false
        stackView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
          contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
          contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
          contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
          contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
          contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
        ])

        NSLayoutConstraint.activate([
          stackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 25),
          stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -25),
          stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
          stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 25)
        ])

        return scrollView
    }

    func makeUpdateButton() -> Button {
        let button = Button()
        button.setTitle("Update", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            button.heightAnchor.constraint(equalToConstant: 45)
        ])

        return button
    }
}
