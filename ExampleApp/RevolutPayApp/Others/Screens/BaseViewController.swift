//
//  BaseViewController.swift
//  RevolutPayApp
//
//  Created by Revolut on 08.07.2022.
//

import UIKit

class BaseViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .systemBackground
        navigationController?.navigationBar.prefersLargeTitles = true
        hideKeyboardOnViewTap()
    }
}

private extension BaseViewController {
    func hideKeyboardOnViewTap() {
        let tapGestureRecognizer = UITapGestureRecognizer(
            target: view,
            action: #selector(view.endEditing(_:))
        )
        tapGestureRecognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGestureRecognizer)
    }
}
