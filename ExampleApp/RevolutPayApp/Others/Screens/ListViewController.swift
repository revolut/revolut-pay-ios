//
//  ListViewController.swift
//  RevolutPayApp
//
//  Created by Revolut on 09.07.2022.
//

import UIKit

final class ListViewController: BaseViewController {
    private let tableView = UITableView(frame: .zero, style: .insetGrouped)
    private lazy var rows = makeRows()
}

extension ListViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpView()
        setUpHierarchy()
        setUpConstraints()
    }
}

private extension ListViewController {
    func setUpView() {
        title = "Revolut Pay Demo"
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        tableView.delegate = self
    }

    func setUpHierarchy() {
        view.addSubview(tableView)
    }

    func setUpConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor)
        ])
    }
}

private extension ListViewController {
    func makeRows() -> [Row] {
        [
            .init(
                title: "Configure SDK",
                onTap: { [weak self] in
                    self?.navigationController?
                        .pushViewController(
                            ConfigurationViewController(),
                            animated: true
                        )
                }
            ),
            .init(
                title: "Checkout Example",
                onTap: { [weak self] in
                    self?.navigationController?
                        .pushViewController(
                            ExampleViewController(),
                            animated: true
                        )
                }
            ),
            .init(
                title: "Button Styles",
                onTap: { [weak self] in
                    self?.navigationController?
                        .pushViewController(
                            ButtonStylesViewController(),
                            animated: true
                        )
                }
            )
        ]
    }
}

extension ListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") else {
            return UITableViewCell()
        }
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.text = rows[indexPath.row].title
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        rows.count
    }
}

extension ListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        rows[indexPath.row].onTap()
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

private extension ListViewController {
    struct Row {
        let title: String
        let onTap: () -> Void
    }
}
