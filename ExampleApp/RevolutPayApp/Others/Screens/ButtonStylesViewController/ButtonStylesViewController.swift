//
//  ButtonStylesViewController.swift
//  RevolutPayApp
//
//  Created by Revolut on 11.07.2022.
//

import UIKit
import RevolutPay

final class ButtonStylesViewController: BaseViewController {
    private let tableView = UITableView(frame: .zero, style: .grouped)
    private let revolutPayKit = RevolutPayKit()
    private let completionPresenter: CompletionPresenterProtocol
    private lazy var sections = makeSections()

    init(completionPresenter: CompletionPresenterProtocol) {
        self.completionPresenter = completionPresenter
        super.init(nibName: nil, bundle: nil)
    }

    convenience init() {
        self.init(completionPresenter: CompletionPresenter())
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ButtonStylesViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        configureRevolutPay()
        setUpView()
        setUpHierarchy()
        setUpConstraints()
    }
}

private extension ButtonStylesViewController {
    func setUpView() {
        title = "Button Styles"

        view.backgroundColor = .systemGray5
        tableView.backgroundColor = .systemGray5
        tableView.separatorStyle = .none

        tableView.register(ButtonStyleTableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        tableView.delegate = self
    }

    func setUpHierarchy() {
        view.addSubview(tableView)
    }

    func setUpConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor)
        ])
    }

    func makeSections() -> [Section] {
        typealias Style = RevolutPayButton.Style

        let variants: [Style.VariantModes] = [
            .init(lightMode: .dark, darkMode: .light),
            .init(lightMode: .light, darkMode: .dark),
            .init(lightMode: .darkOutlined, darkMode: .lightOutlined),
            .init(lightMode: .lightOutlined, darkMode: .darkOutlined)
        ]
        let sizes: [Style.Size] = [.large, .medium, .small, .extraSmall]
        let attachmentStyles: [Style.AttachmentStyle?] = [.init(), nil]
        let radii: [Style.Radius] = [.rounded, .medium, .small, .none]

        return [
            .init(title: "Variants", styles: variants.map { .init(variants: $0) }),
            .init(title: "Attachment", styles: attachmentStyles.map { .init(attachmentStyle: $0) }),
            .init(title: "Radius", styles: radii.map { .init(radius: $0) }),
            .init(title: "Size", styles: sizes.map { .init(size: $0, radius: .none) })
        ]
    }

    func configureRevolutPay() {
        RevolutPayKit.configure(
            with: .init(
                merchantPublicKey: Defaults.merchantPublicKey,
                environment: Defaults.environment
            )
        )
    }
}

extension ButtonStylesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        sections[section].styles.count
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        sections.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: "cell",
            for: indexPath
        ) as? ButtonStyleTableViewCell else {
            return UITableViewCell()
        }

        cell.update(
            withStyle: sections[indexPath.section].styles[indexPath.row],
            revolutPayKit: revolutPayKit,
            createOrder: { createOrderHandler in
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
                    guard let orderToken = Defaults.orderToken else {
                        createOrderHandler.cancel()
                        return
                    }
                    createOrderHandler.set(orderToken: orderToken)
                }
            },
            completion: { [weak self] result in
                self?.completionPresenter.showCompletionAlert(for: result)
                print(result)
            }
        )

        return cell
    }
}

extension ButtonStylesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 27)
        label.text = sections[section].title
        label.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(label)

        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 12),
            label.trailingAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -25),
            label.bottomAnchor.constraint(lessThanOrEqualTo: view.bottomAnchor, constant: -12),
            label.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 25)
        ])

        return view
    }
}

private extension ButtonStylesViewController {
    struct Section {
        let title: String
        let styles: [RevolutPayButton.Style]
    }
}
