//
//  ButtonStyleTableViewCell.swift
//  RevolutPayApp
//
//  Created by Revolut on 11.07.2022.
//

import UIKit
import RevolutPay

final class ButtonStyleTableViewCell: UITableViewCell {
    private var button: RevolutPayButton?

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func update(
        withStyle style: RevolutPayButton.Style,
        revolutPayKit: RevolutPayKit,
        createOrder: @escaping RevolutPayKit.CreateOrderCallback,
        completion: @escaping (Result<Void, RevolutPayKit.Error>) -> Void
    ) {
        addButton(
            revolutPayKit: revolutPayKit,
            style: style,
            createOrder: createOrder,
            completion: completion
        )
    }
}

private extension ButtonStyleTableViewCell {
    func addButton(
        revolutPayKit: RevolutPayKit,
        style: RevolutPayButton.Style,
        createOrder: @escaping RevolutPayKit.CreateOrderCallback,
        completion: @escaping (Result<Void, RevolutPayKit.Error>) -> Void
    ) {
        button?.removeFromSuperview()

        button = makeButton(
            revolutPayKit: revolutPayKit,
            style: style,
            createOrder: createOrder,
            completion: completion
        )
        guard let button = button else { return }

        button.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(button)

        NSLayoutConstraint.activate([
            button.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 12),
            button.trailingAnchor.constraint(lessThanOrEqualTo: contentView.trailingAnchor, constant: -25),
            button.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -12),
            button.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 25)
        ])
    }

    func makeButton(
        revolutPayKit: RevolutPayKit,
        style: RevolutPayButton.Style,
        createOrder: @escaping RevolutPayKit.CreateOrderCallback,
        completion: @escaping (Result<Void, RevolutPayKit.Error>) -> Void
    ) -> RevolutPayButton {
        revolutPayKit.button(
            style: style,
            returnURL: Defaults.returnURL,
            createOrder: createOrder,
            completion: completion
        )
    }
}
