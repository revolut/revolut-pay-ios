//
//  Button.swift
//  RevolutPayApp
//
//  Created by Revolut on 11.07.2022.
//

import UIKit

final class Button: UIButton {
    override var isEnabled: Bool {
        didSet {
            backgroundColor = isEnabled ? .systemBlue : .systemGray3
        }
    }

    init() {
        super.init(frame: .zero)
        backgroundColor = .systemBlue
        layer.cornerRadius = 15
        titleLabel?.font = .boldSystemFont(ofSize: 17)
        setTitleColor(.white, for: .normal)
        setTitleColor(.white.withAlphaComponent(0.4), for: .highlighted)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
