//
//  SceneDelegate.swift
//  RevolutCardPaymentsApp
//
//  Created by Revolut on 09.02.2022.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?

    func scene(
        _ scene: UIScene,
        willConnectTo session: UISceneSession,
        options connectionOptions: UIScene.ConnectionOptions
    ) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        let window = UIWindow(windowScene: windowScene)

        let navigationContrller = UINavigationController(rootViewController: ListViewController())
        navigationContrller.pushViewController(ConfigurationViewController(), animated: true)

        window.rootViewController = navigationContrller
        window.makeKeyAndVisible()
        self.window = window
    }
}
