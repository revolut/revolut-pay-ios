//
//  Defaults.swift
//  RevolutPayApp
//
//  Created by Revolut on 08.07.2022.
//

import Foundation
import RevolutPay

enum Defaults {
    // Set your public merchant key if you want a default value.
    static var merchantPublicKey = ""

    // Set an order token if you want a default value.
    static var orderToken: String?

    // Set a different value if you want another default environment.
    static var environment: RevolutPayKit.Environment = .production

    // The Example project supports the `exampleapp` URL scheme.
    // As the documentation states, you must provide a Universal Link or URL based scheme
    // to your app in your own implementation.
    static let returnURL = "exampleapp://revolut-pay"
}
