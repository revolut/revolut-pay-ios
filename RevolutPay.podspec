Pod::Spec.new do |spec|
    spec.name = 'RevolutPay'
    spec.version = '2.0.1'
    spec.summary = 'Revolut - RevolutPay'
    spec.homepage = 'https://bitbucket.org/revolut/revolut-pay-ios'
    spec.source = { :git => 'https://bitbucket.org/revolut/revolut-pay-ios', :tag => spec.version.to_s }
    spec.license = { :type => 'Custom', :file => 'LICENSE' }
    spec.author = { 'Revolut' => 'team@revolut.com' }
    spec.swift_version = '5.0'
    spec.static_framework = true
    spec.ios.deployment_target = '13.0'

    spec.resources = [
        'RevolutPay/RevolutPay.bundle',
    ]

    spec.vendored_frameworks = [
        'RevolutPay/RevolutPay.xcframework',
    ]
end